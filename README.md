# Topic Models of CORD-19 Dataset 

To reproduce the [models](./out/) of the papers’ abstracts, you need to have [R](https://www.r-project.org/) running on your system. Furthermore, you need to install R packages [jsonlite](https://cran.r-project.org/web/packages/jsonlite/index.html) as well as [topmodelr](https://github.com/herreio/topmodelr) via [devtools](https://cran.r-project.org/web/packages/devtools/readme/README.html):

```R
# to install jsonlite:
install.packages("jsonlite")
# to install devtools if you don’t have it:
install.packages("devtools")
# to install topmodelr:
devtools::install_github("herreio/topmodelr")
```

Have a look at the [README](./data/README.md) in the `data`-folder before continuing... Finally, you can start the modeling routine:

```sh
Rscript src/routine.R
```

To get an impression of the results, the [visualizations](https://herreio.gitlab.io/cord19-models/) created with the help of [LDAvis](https://cran.r-project.org/web/packages/LDAvis/README.html) are a good starting point.
