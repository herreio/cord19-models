# CORD-19 Dataset (2020-04-03)

Please [download](https://pages.semanticscholar.org/coronavirus-research) and unzip the full dataset (`CORD-19-research-challenge.zip`) into this folder.

```
data/
├── biorxiv_medrxiv
│   └── ...
├── comm_use_subset
│   └── ...
├── custom_license
│   └── ...
├── noncomm_use_subset
│   └── ...
├── COVID.DATA.LIC.AGMT.pdf
├── json_schema.txt
├── metadata.csv
├── metadata.readme
└── README.md
```
