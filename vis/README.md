# Topic Model Visualization Data

In this directory you can find data extracted from a parent topic model `lda_abstracts_t10` with ten topics as well as data extracted from children models `lda_subset-topic-{1,2,3,...,10}_t10` with in turn ten topics. Each child model is associated with one of the ten topics from the parent model. This way we have data for 10 + 10 \* 10 topics, i.e. 10 on the first and 100 on the second layer.

The frequency of the topics and their coordinates on the first layer can be found in `topics-first-layer.json`. For the second layer, please have a look at `topics-second-layer.json`. The associations between papers, most likely topic and probability of the papers to be in the most likely topic are given in `papers-first-layer.json` and `papers-second-layer.json`. The paper ids and corresponding titles of the papers can be found in `meta-ids-titles.json`.
